package utils;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.read.biff.File;

import java.io.*;

public class AnalysisExcel {
 
 Workbook workbook = null;
 String Inputfile = "C:\\Users\\xxxx.xls";
 String outputfile ="D:\\excelPath\\xxxx.txt";
 
 public void ToAnalysisExcel() {
 // Unable to recognize OLE stream 不支持xlsx格式 支持xls格式
 
 try {
  FileInputStream fileInputStream = new FileInputStream(Inputfile);
  workbook = Workbook.getWorkbook(fileInputStream);
  FileOutputStream fileOutputStream = new FileOutputStream(outputfile);
  BufferedOutputStream bw = new BufferedOutputStream(fileOutputStream); //输出语句
 
  Sheet readfirst = workbook.getSheet(0);
  int rows = readfirst.getRows();
  int clomns = readfirst.getColumns();
  System.out.println("row:" + rows);
  System.out.println("clomns:" + clomns);
 
  for(int i =1;i<rows;i++) {
  Cell[] cells = readfirst.getRow(i); //循环得到每一行的单元格对象
 
  //根据每一个单元格对象的到里面的值
  String brandNum= cells[0].getContents(); 
  String name = cells[1].getContents();
  String birthDay =cells[2].getContents();
  System.out.println("brandNum:"+brandNum+",name:"+name+",birthDay:"+birthDay);
  
   //将得到的值放在一个我需要的格式的string对象中
 
  String output = "\n"+birthDay+"\n" +
   "{\n" +
   " \"brandColor\": 500000,\n" +
   " \"brandNumber\": \""+brandNum+"\",\n" +
   " \"deviceCode\": \""+name+"\",\n" +
   " \"simCard\": \""+birthDay+"\"\n" +
   "}"+
    "\n";
   System.out.println(output);
  //write and flush到 我需要的文件中去，flush后才能成功
  byte[] outputbyte = new String(output).getBytes();
  bw.write(outputbyte);
 
  bw.flush();
  }
 
 
 } catch (FileNotFoundException e) {
  e.printStackTrace();
 } catch (IOException e) {
  e.printStackTrace();
 } catch (BiffException e) {
  e.printStackTrace();
 }

 }

 public static void main(String[] args) {
  AnalysisExcel analysisExcel = new AnalysisExcel();

  analysisExcel.ToAnalysisExcel();
 }
 
}